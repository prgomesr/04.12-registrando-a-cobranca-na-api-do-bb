package br.com.ozeano.curso.api.bb.infra.model.input;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(Include.NON_NULL)
public class PagadorInput {

	private Integer tipoInscricao;
	private String numeroInscricao;
	private String nome;
	private String endereco;
	private Long cep;
	private String cidade;
	private String bairro;
	private String uf;
	private String telefone;

}
