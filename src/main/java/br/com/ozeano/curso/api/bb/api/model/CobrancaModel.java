package br.com.ozeano.curso.api.bb.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CobrancaModel {

	private String clientId;
	private String clientSecret;
	private String appKey;
	
}
