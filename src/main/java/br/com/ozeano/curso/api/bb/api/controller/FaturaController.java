package br.com.ozeano.curso.api.bb.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ozeano.curso.api.bb.api.model.CobrancaModel;
import br.com.ozeano.curso.api.bb.domain.service.FaturaService;
import br.com.ozeano.curso.api.bb.infra.model.BoletoRegistrado;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

@RestController
@RequestMapping("faturas")
public class FaturaController {

	@Autowired
	private FaturaService service;

	@GetMapping("{faturaId}")
	public CobrancaInput transformar(@PathVariable Long faturaId) {
		return service.transformarFaturaEmCobranca(faturaId);
	}
	
	@PostMapping("{faturaId}")
	public BoletoRegistrado registrar(@PathVariable Long faturaId, @RequestBody CobrancaModel model) {
		return service.registrarCobranca(faturaId, model);
	}

}
