package br.com.ozeano.curso.api.bb.domain.model;

public enum SituacaoFatura {

	PAGA, NAO_PAGA, CANCELADA;
	
}
